---
title: "关于"
date: 2017-08-20T21:38:52+08:00
lastmod: 2017-08-28T21:41:52+08:00
menu: "main"
weight: 50

# you can close something for this content if you open it in config.toml.
comment: false
mathjax: false
---

这个是我的个人博客记录地方。


我是一个Java后端研发工程师，学习的东西比较杂，如果你对我学习的东西感兴趣可以进行关注。
